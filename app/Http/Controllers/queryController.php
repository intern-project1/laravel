<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Query;
use Exception;
use Illuminate\Http\Request;

class queryController extends Controller
{
    //
    public function createQuery(Request $request)
    {

        try {

            $request->validate([
                "menu" => "max:255",
                "sub_menu" => "max:255",
                "title" => "max:1000",
                "content" => "max:10000",
                "reference" => "max:10000",
                "project_id" => "required",
            ], [

                "menu.max" => "255 chars",
                "sub_menu.max" => "255 chars",
                "title.max" => "1k chars",
                "content.max" => "10k chars",
                "reference.max" => "10k chars",
                "project_id.required" => "project_id is required",
            ]);

            $query = new Query();
            $query->user_id = $request->user()->id;
            $query->project_id = $request->project_id;
            $query->menu = $request->menu;
            $query->sub_menu = $request->sub_menu;
            $query->title = $request->title;
            $query->content = $request->content;
            $query->reference = $request->reference;
            $query->save();

            return response()->json(
                [
                    "success" => true,
                    "message" => "query created",
                ]
            );
        } catch (Exception $e) {
            return response()->json(["success" => false, "error" => $e]);
        }

    }

    public function viewQuery(Request $request, $query_id)
    {
        $user = $request->user();
        $query = Query::where('id', $query_id)->first();
        $answersLength = Answer::select('id')->where('query_id', $query_id)->get();
        return response()->json([
            "success" => true,
            "query" => $query,
            "user" => $user,
            "answersLength" => $answersLength,
        ]);
    }

    public function viewQueryIdsByProjectId(Request $request, $project_id)
    {
        $user = $request->user();
        $queries = Query::select('id', 'created_at')
            ->orderBy('created_at', 'desc')
            ->where('project_id', $project_id)
            ->get();
        return response()->json(["success" => true, "queries" => $queries]);
    }

}
