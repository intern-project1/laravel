<?php

namespace App\Http\Controllers;

use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class userController extends Controller
{
    //

    public function signup(Request $request)
    {

        $userExists = User::where('username', $request->username)->first();
        if ($userExists != null) {
            return response()->json(["success" => false, "message" => "username already registered"], 400);
        }
        $userExists = User::where('email', $request->email)->first();
        if ($userExists != null) {
            return response()->json(["success" => false, "message" => "email alredy registered"], 400);
        }
        try {
            $user = new User();
            $user->username = $request->username;
            $user->email = $request->email;
            $user->password = Hash::make($request->password, ['rounds' => 10]);
            $user->save();
            $token = $user->createToken(env('TOKEN_KEY', 'fallback_token_key'));
        } catch (Exception $e) {
            return response()->json(["error" => $e, "success" => false, "message" => "error occurred"], 400);
        }
        return response()->json(['success' => true, 'token' => $token->plainTextToken], 201);

    }

    public function login(Request $request)
    {
        try {
            $user = User::select('id', 'email', 'password')->where('email', $request->email)->first();
            if (!$user) {
                return response()->json(["success" => false, "message" => "email not found"], 404);
            }
            if (!Hash::check($request->password, $user->password)) {
                return response()->json([
                    "success" => false,
                    "message" => "password is incorrect",
                    "hashed" => $user->password,
                    "user" => $user,
                ], 400);
            }
            $token = $user->createToken(env('TOKEN_KEY', 'fallback_token_key'));

        } catch (Exception $e) {
            return response()->json(["error" => $e]);
        }
        return response()->json(["success" => true, "token" => $token->plainTextToken]);
    }

    public function logout(Request $request)
    {

        $user = User::where('id', $request->user()->id)->first();
        if ($user == null) {
            return response()->json(["success" => false, "message" => "user not found"], 404);
        }
        $user->tokens()->delete();

        return response()->json(["success" => true, "message" => "logout success"]);
    }

}
