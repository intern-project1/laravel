<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Query;
use Illuminate\Http\Request;

class answerController extends Controller
{
    //
    public function createAnswer(Request $request, $query_id)
    {
        $validator = $request->validate([
            "answer" => "required|max:10000",
        ], [

            "answer.required" => "required",
            "answer.maxlength" => "10k chars",
        ]);
        $query = Query::where('id', $query_id)->first();
        if (!$query) {
            return response()->json(["success" => false, "message" => "Query not found"], 404);
        }
        $answer = new Answer();
        $answer->query_id = $query->id;
        $answer->user_id = $request->user()->id;
        $answer->answer = $request->answer;
        $answer->save();
        return response()->json(["success" => true, "message" => "answer created"]);
    }

    public function viewAnswersByQueryId(Request $request, $query_id)
    {
        $answers = Answer::orderBy('created_at', 'desc')
            ->where('query_id', $query_id)
            ->get();
        return response()->json(["success" => true, "answers" => $answers]);
    }
}
