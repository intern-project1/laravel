<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Project;
use App\Models\Query;
use Exception;
use Illuminate\Http\Request;

class projectController extends Controller
{
    //
    public function createProject(Request $request)
    {
        try {
            $validator = $request->validate([
                "project_name" => "required|max:255",
                "project_description" => "required|max:10000",
                "project_slug" => "required|max:1000",
            ], [

                "project_name.maxlength.required" => " 255 chars",
                "project_description.maxlength.required" => " 10k characters ",
                "project_slug.maxlength.required" => " 1k characters",
            ]);
            $project = new Project();
            $project->project_name = $request->project_name;
            $project->project_slug = $request->project_slug;
            $project->project_description = $request->project_description;
            $project->save();
            return response()->json(["success" => true, "message" => "project created"]);
        } catch (Exception $e) {
            return response()->json(["error" => $e], 400);
        }
    }

    public function viewProjects()
    {
        try {
            $projects = Project::orderBy('created_at', 'desc')->get();
            return response()->json(["success" => true, "projects" => $projects]);
        } catch (Exception $e) {
            return response()->json(["success" => false, "error" => $e]);
        }
    }

    public function updateProject(Request $request, $id)
    {
        try {
            $project = Project::where('id', $id)->first();
            $project->project_name = $request->project_name ? $request->project_name : $project->project_name;

            $project->project_slug = $request->project_slug ? $request->project_slug : $project->project_slug;

            $project->project_description = $request->project_descriptio ? $request->project_description : $project->project_description;

            $project->save();
            return response()->json(["success" => true, "message" => "project updated"]);
        } catch (Exception $e) {
            return response()->json(["error" => $e], 400);
        }
    }

    public function viewProject($id)
    {
        try {
            $project = Project::where('id', $id)->first();

            return response()->json(["success" => true, "project" => $project], 200);
        } catch (Exception $e) {
            return response()->json(["error" => $e]);
        }
    }

    public function viewProjectDiscussion(Request $request, $project_id)
    {
        $project = Project::where('id', $project_id)->first();
        $queries = Query::with('user')
            ->orderBy('created_at', 'desc')
            ->where('project_id', $project_id)
            ->get();

        foreach ($queries as $query) {
            $answers = Answer::with('user')
                ->orderBy('created_at', 'desc')
                ->where('query_id', $query->id)
                ->get();
            $query["answers"] = $answers;

        }
        $project["queries"] = $queries;
        return response()->json(["success" => true, "project" => $project]);
    }

}
