<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Query extends Model
{
    use HasFactory;

    public function project()
    {
        return $this->belongsTo('App\Models\Project', 'project_id');
    }

    public function answers()
    {
        return $this->hasMany('App\Models\Answer', 'query_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

}
