<?php

use App\Http\Controllers\answerController;
use App\Http\Controllers\projectController;
use App\Http\Controllers\queryController;
use App\Http\Controllers\userController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Route::get('/login', function (Request $request) {
//     return $request->user();
// });

// USERS ROUTE
Route::post('users/signup', [userController::class, 'signup']);
Route::post('users/login', [userController::class, 'login']);
Route::middleware('auth:sanctum')->put('users/logout', [userController::class, 'logout']);

// PROJECTS ROUTE
Route::middleware('auth:sanctum')->get('projects/view-projects', [projectController::class, 'viewProjects']);
Route::middleware('auth:sanctum')->post('projects/create', [projectController::class, 'createProject']);
// Route::middleware('auth:sanctum')->post('projects/update/{project_id}', [projectController::class, 'updateProject']);
Route::middleware('auth:sanctum')->get('projects/view-project/{project_id}', [projectController::class, 'viewProject']);

// QUERY ROUTE
Route::middleware('auth:sanctum')->post('queries/create', [queryController::class, 'createQuery']);
// Route::middleware('auth:sanctum')->get('queries/view-query/{query_id}', [queryController::class, 'viewQuery']);
// Route::middleware('auth:sanctum')->get('queries/view-queries/{project_id}', [queryController::class, 'viewQueryIdsByProjectId']);

// ANSWERS ROUTE
Route::middleware('auth:sanctum')->post('answers/create/{query_id}', [answerController::class, 'createAnswer']);
// Route::middleware('auth:sanctum')->get('answers/view-answers/{query_id}', [answerController::class, 'viewAnswersByQueryId']);

//  ENTIRE DISCUSSION OF A PROJECT
Route::middleware('auth:sanctum')->get('projects/view-discussion/{project_id}', [projectController::class, 'viewProjectDiscussion']);
