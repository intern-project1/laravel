import Axios from "axios";
import React, { useEffect } from "react";
import ReactDOM from "react-dom";

function Example() {
    useEffect(() => {
        Axios.post(
            "http://localhost:8000/api/users/signup",
            {
                email: "some@email.com",
                password: "password"
            },
            {
                headers: {
                    "X-CSRF-TOKEN": document.head
                        .querySelector('meta[name="csrf-token"]')
                        .getAttribute("content")
                }
            }
        )
            .then(res => console.log(res))
            .catch(err => console.log(err.response));
        return () => {};
    }, []);
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-header">Example Component</div>

                        <div className="card-body">
                            I'm an example component!
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Example;

if (document.getElementById("example")) {
    ReactDOM.render(<Example />, document.getElementById("example"));
}
