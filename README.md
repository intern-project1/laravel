# Back End

## Installation

-   clone the repo

```
git clone https://gitlab.com/intern-project1/laravel.git
```

-   move to working directory

```
cd laravel
```

-   install dependencies

```
composer install
```

_**please run apache and mysql in xampp and create a database named "laravel" before running the command below**_

-   start the development server

```
php artisan serve
```

_Laravel development server started on port 8000_

## Routes

```
/api/users/signup
/api/users/login
/api/users/logout   (auth_token : required)

/api/projects/create    (auth_token : required)
/api/projects/view-projects     (auth_token : required)
/api/projects/view-project/{project_id}     (auth_token : required)
/api/projects/view-discussion/{project_id}      (auth_token : required)

/api/queries/create     (auth_token : required)

/api/answers/create/{query_id}      (auth_token : required)

```
